<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Formulario2;
use kartik\mpdf\Pdf;
use yii\data\ActiveDataProvider;
use yii\helpers\FileHelper;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionFormulario1()
    {
        $model = new \app\models\Formulario1();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                return $this->render('solucion1', [
                    'model' => $model,
                ]);
            }
        }

        return $this->render('formulario1', [
            'model' => $model,
        ]);
    }

    public function actionFormulario2()
    {
        $model = new \app\models\Formulario2();

        $dataProvider = new ActiveDataProvider([
            'query' => Formulario2::find(),
        ]);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $model->save()) {
                return $this->render('solucion2', [
                    'model' => $model,
                    'dataProvider' => $dataProvider,
                ]);
            }
        }

        return $this->render('formulario2', [
            'model' => $model,
        ]);
    }
    public function actionFormulario3()
    {
        $model = new \app\models\Formulario3();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                return $this->render('solucion3', [
                    'model' => $model,
                ]);
            }
        }

        return $this->render('formulario3', [
            'model' => $model,
        ]);
    }

    public function actionFormulario4()
    {
        $model = new \app\models\Formulario4();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                return $this->render('solucion4', [
                    'model' => $model,
                ]);
            }
        }

        return $this->render('formulario4', [
            'model' => $model,
        ]);
    }

    public function actionFormulario5()
    {
        $model = new \app\models\Formulario5();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                return $this->render('solucion5', [
                    'model' => $model,
                ]);
            }
        }

        return $this->render('formulario5', [
            'model' => $model,
        ]);
    }

    public function actionFicheros()
    {
        // Con FileHelper::findfiles obtenemos un array enumerado con todos los archivos de la carpeta

        // los pdfs tienes este nombre => './pdfs\JQuery-Diapositivas.pdf'
        // nos queremos quedar solo con el nombre del pdf
        // leemos donde está la \ y nos quedamos con lo que está por delante
        $ficheros = FileHelper::findFiles('./pdfs');
        foreach ($ficheros as $indice => $fichero) {
            $ficheros[$indice] = substr($fichero, strpos($fichero, '\\') + 1);
        }

        return $this->render('ficheros', compact('ficheros'));
        // compact es como poner un array ['ficheros' => $ficheros]
        // return $this->render('ficheros', [
        //     'ficheros' => $ficheros,
        // ]);
    }
}
