<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Formulario5 $model */
/** @var ActiveForm $form */
?>
<div class="site-formulario5">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'activado')->checkbox() ?>
    <?= $form->field($model, 'mes')->dropDownList($model->meses, ['prompt' => 'Selecciona mes']) ?>
    <?= $form->field($model, 'opcion')->radioList($model->opciones, ['prompt' => 'Selecciona opciones']) ?>
    <?= $form->field($model, 'dias')->listBox($model->mostrardias, ['multiple' => 'multiple']) ?>
    <?= $form->field($model, 'id')->input('number', ['placeholder' => 'Introduce ID']) ?>
    <?= $form->field($model, 'fechaReserva')->input('date') ?>
    <?= $form->field($model, 'archivopdf')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-formulario5 -->