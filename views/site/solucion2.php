<link rel="stylesheet" href="@web/css/estilo.css">
<?php

use yii\grid\GridView;
use yii\widgets\DetailView;



?>
<h3 class="text-align=center">DATO INTRODUCIDO</h3>

<?php
echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        [
            'label' => 'Día',
            'value' => ($model->dia + 1),
        ],
        'mes',
        'poblacion',
    ]
]);


?>

<h3 class="text-align=center">DATOS</h3>

<?php

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'dia',
        'mes',
        'poblacion',
    ],

]);
