<?php

use yii\widgets\DetailView;

echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'nombre',
        'edad',
        'mes',
        'dia',
        'categoria',
        'listaAficiones',
        // 'listaOpciones',
        [
            'label' => 'Opciones',
            "value" => function ($model) {
                return join(",", $model->opciones);
            }
        ],
        [
            'label' => 'Fecha de alta',
            'value' => $model->fechaEspana(),
        ],
    ],
])

?>
</div>