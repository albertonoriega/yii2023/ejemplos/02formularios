<?php

use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Formulario4 $model */
/** @var ActiveForm $form */
?>
<div class="site-formulario4">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['prompt' => 'Introduce nombre']) ?>
    <?= $form->field($model, 'apellidos')->textInput(['prompt' => 'Introduce apellidos']) ?>
    <?= $form->field($model, 'fechaNacimiento')->input('date') ?>
    <?= $form->field($model, 'correo')->input('email') ?>
    <?= $form->field($model, 'poblacion')->dropDownList($model->poblaciones, ['prompt' => 'Selecciona población']) ?>
    <?= $form->field($model, 'mesesAcceso')->checkboxList($model->mesesacceso) ?>

    <div class="form-group">
        <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-formulario4 -->