<?php

use yii\helpers\Url;
use yii\widgets\DetailView;
?>
<link rel="stylesheet" href="<?= Url::to('@web/css/detailStyles.css') ?>">

<div class=contenedorDetail>
    <?php
    echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'numero1',
            'numero2',
            [
                'label' => 'Suma',
                'value' => function ($model) {
                    return $model->operaciones('+');
                }
            ],
            [
                'label' => 'Resta',
                'value' => function ($model) {
                    return $model->operaciones('-');
                }
            ],
            [
                'label' => 'Producto',
                'value' => function ($model) {
                    return $model->operaciones('*');
                }
            ],
            [
                'label' => 'División',
                'value' => function ($model) {
                    return $model->operaciones('/');
                }
            ],
        ],
        'options' => [
            'class' => 'table table-bordered detail-view'
        ]
    ]);
    ?>
</div>

<div class="contenedorDetail">

    <?php

    echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'descripcion',
            [
                'label' => 'Número de vocales de la descripción',
                'value' => function ($model) {
                    return $model->contarVocales();
                }
            ],
        ],
        'options' => [
            'class' => 'table table-bordered detail-view'
        ]
    ])

    ?>
</div>
<?php
echo "Suma: {$model->suma()}";

?>

<h2> La suma de los dos números es: <?= $model->suma() ?> </h2>
<h2> La resta de los dos números es: <?= $model->resta() ?> </h2>
<h2>Número de vocales: <?= $model->contarVocales() ?></h2>
<h2>Número de vocales: <?= $model->contarVocales1() ?></h2>
<h2>Número de vocales: <?= $model->contarVocales2() ?></h2>