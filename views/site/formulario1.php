<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Formulario1 $model */
/** @var ActiveForm $form */
?>
<div class="site-formulario1">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'numero1')->input('number') ?>
    <?= $form->field($model, 'numero2')->input('number') ?>
    <?= $form->field($model, 'descripcion')->textInput(['placeholder' => 'Introduce descripcion']) ?>

    <div class="form-group">
        <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-formulario1 -->