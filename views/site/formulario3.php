<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Formulario3 $model */
/** @var ActiveForm $form */
?>
<div class="site-formulario3">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['placeholder' => 'Introduce nombre completo']) ?>
    <?= $form->field($model, 'edad')->input('number', ['placeholder' => 'Introduce edad']) ?>
    <?= $form->field($model, 'mes')->dropDownList($model->meses, ['prompt' => 'Selecciona mes']) ?>
    <?= $form->field($model, 'dia')->listBox($model->dias) ?>
    <?= $form->field($model, 'categoria')->radioList($model->categorias, ['prompt' => 'Selecciona categoría']) ?>

    <?= $form->field($model, 'aficiones')->checkboxList($model->mostraraficiones) ?>
    <?= $form->field($model, 'opciones')->listBox($model->mostraropciones, ['multiple' => 'multiple']) ?>
    <?= $form->field($model, 'fechaAlta')->input('date') ?>

    <div class="form-group">
        <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-formulario3 -->