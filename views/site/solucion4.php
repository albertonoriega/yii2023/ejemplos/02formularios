<?php

use yii\widgets\DetailView;

echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'nombre',
        'apellidos',
        [
            'label' => 'Fecha de nacimiento',
            'value' => $model->fechaEspana(),
        ],
        'correo',
        'poblacion',
        'listameses',
    ]
]);
