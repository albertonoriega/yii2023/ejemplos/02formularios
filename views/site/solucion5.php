<?php

use yii\widgets\DetailView;

echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'activado:boolean',
        'mes',
        'opcion',
        'Listadias',
        'id',
        'fechaReserva',
        [
            'attribute' => 'Mostrar pdf',
            'format' => 'raw',
            'value' => function ($model) {
                return \yii\helpers\Html::a('Mostrar PDF ' . $model->archivopdf->name, '@web/pdfs/' . $model->archivopdf->name,  ["class" => "btn btn-danger", 'target' => '_blank',]);
            }
        ]

    ]
]);
