<?php

/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent mt-5 mb-5">
        <h1 class="display-4">Ejemplo de formularios</h1>

        <div class="row">
            <div class="col-6 mt-4">
                <h3>Formulario 1</h3>
                <p class="lead">Crear el modelo de un formulario</p>
                <p class="lead">Generar form usando gii</p>
                <p class="lead">Métodos en el modelo</p>
                <p class="lead">Mostrar datos usando DetailView</p>
            </div>
            <div class="col-6 mt-4">
                <h3>Formulario 2</h3>
                <p class="lead">Crear el modelo del formulario</p>
                <p class="lead">Generar form usando gii</p>
                <p class="lead">Métodos en el modelo para hacer lista desplegables y ListBox</p>
                <p class="lead">Mostrar datos usando DetailView y los registros de la BBDD usando GridView</p>
                <p class="lead"><span style="font-weight: bold;color:blue">Guardar los datos introducidos en la BBDD</span></p>
            </div>
            <div class="col-6 mt-4">
                <h3>Formulario 3</h3>
                <p class="lead">Crear el modelo de un formulario</p>
                <p class="lead">Generar form usando gii</p>
                <p class="lead">Métodos en el modelo <span style="font-weight: bold;color:blue">DropdownList, botones de radio, CheckboxList, ListBox multiple</span< /p>
                        <p class="lead">Mostrar los datos en un DetailView</p>
                        <p class="lead"><span style="font-weight: bold;color:blue">Mostrar la fecha en el detailView como DD/MM/YYYY</span></p>
            </div>
            <div class="col-6 mt-4">
                <h3>Formulario 4</h3>
                <p class="lead">Crear el modelo de un formulario</p>
                <p class="lead">Generar form usando gii</p>
                <p class="lead"><span style="font-weight: bold;color:blue">Rules para email y fecha máxima</span></p>
                <p class="lead">Métodos en el modelo DropdownList, CheckboxList</p>
                <p class="lead">Mostrar los datos en un DetailView</p>
                <p class="lead">Mostrar la fecha en el detailView como DD/MM/YYYY</p>
            </div>
            <div class="col-6 mt-4">
                <h3>Formulario 5</h3>
                <p class="lead">Crear el modelo de un formulario</p>
                <p class="lead">Generar form usando gii</p>
                <p class="lead">Métodos en el modelo DropdownList, Checkbox, RadioList <span style="font-weight: bold;color:blue">Subir PDF </span></p>
                <p class="lead">Mostrar los datos en un DetailView <span style="font-weight: bold;color:blue"> con un botón para ver el PDF</span></p>
                <p class="lead">Al darle el boton abrimos el PDF</p>
            </div>
            <div class="col-6 mt-4">
                <h3>Ver ficheros</h3>
                <p class="lead">Generamos una vista que nos muestra todos los pdfs</p>
                <p class="lead">Para mostrarlos hacemos un bucle usando <span style="font-weight: bold;color:blue">FileHelper::findFiles </span></p>
                <p class="lead">Al hacer clic en el boton de los pdfs, se abre el pdf</p>
            </div>
        </div>
    </div>


</div>