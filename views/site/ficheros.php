<?php

use yii\helpers\Html;

foreach ($ficheros as $fichero) {
    echo '<div>';
    echo Html::a($fichero, '@web/pdfs/' . $fichero, ['class' => 'btn btn-info mt-2 p-3 d-block', 'target' => '_blank']);
    echo '</div>';
}
