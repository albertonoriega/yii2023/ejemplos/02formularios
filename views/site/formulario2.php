<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Formulario2 $model */
/** @var ActiveForm $form */
?>
<div class="site-formulario2">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dia')->input('number') ?>
    <?= $form->field($model, 'mes')->dropDownList($model->meses, ['prompt' => 'Seleciona un mes']) ?>
    <?= $form->field($model, 'poblacion')->listBox($model->poblaciones) ?>

    <div class="form-group">
        <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-formulario2 -->