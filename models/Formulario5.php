<?php

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Description of Formulario5
 */
class Formulario5 extends Model
{

    public ?int $activado = null;
    public ?string $mes = null;
    public ?string $opcion = null;
    public array $dias = [];
    public ?int $id = null;
    public ?string $fechaReserva = null;
    public $archivopdf;

    public function attributeLabels(): array
    {
        return [
            "activado" => "Activado",
            "mes" => "Mes de reserva",
            "opcion" => "Opciones a elegir",
            "dias" => "Días",
            "id" => "ID",
            "fechaReserva" => "Fecha de reserva",
            "archivopdf" => "Selecciona PDF",
        ];
    }

    public function rules(): array
    {
        return [
            [["activado", "mes",  "opcion", "dias", "id", "fechaReserva",], "required"],
            [['id'], 'number'],
            [['fechaReserva'], 'date', 'format' => 'yyyy-MM-dd'],
            [['activado'], 'boolean'],
            [['archivopdf'], 'file', 'maxSize' => 1024 * 1024 * 1, 'skipOnEmpty' => false, 'extensions' => 'pdf'],
        ];
    }

    public function getMeses(): array
    {
        return [
            'Enero' => 'Enero',
            'Febrero' => 'Febrero',
            'Marzo' => 'Marzo',
            'Abril' => 'Abril',
            'Mayo' => 'Mayo',
            'Junio' => 'Junio',
            'Julio' => 'Julio',
            'Agosto' => 'Agosto',
            'Septiembre' => 'Septiembre',
            'Octubre' => 'Octubre',
            'Noviembre' => 'Noviembre',
            'Diciembre' => 'Diciembre'
        ];
    }

    public function getOpciones(): array
    {
        return [
            'opcion1' => 'Opción 1',
            'opcion2' => 'Opción 2',
            'opcion3' => 'Opción 3',
        ];
    }

    public function getMostrardias(): array
    {
        return [
            'lunes' => 'Lunes',
            'martes' => 'Martes',
            'miercoles' => 'Miercoles',
            'jueves' => 'Jueves',
            'viernes' => 'Viernes',
            'sabado' => 'Sábado',
            'domingo' => 'Domingo',
        ];
    }

    public function getListadias(): string
    {
        return join(" , ", $this->dias);
    }

    public function beforeValidate(): bool
    {
        $this->archivopdf = UploadedFile::getInstance($this, "archivopdf");
        return true; // este metodo te pide que devuelvas true. Si no devuelves true no hace nada
    }

    public function afterValidate(): bool
    {

        $this->subirArchivo();
        return true; // este metodo te pide que devuelvas true. Si no devuelves true no hace nada
    }


    public function subirArchivo(): bool
    {
        $this->archivopdf->saveAs('pdfs/' . $this->archivopdf->name);
        return true;
    }
}
