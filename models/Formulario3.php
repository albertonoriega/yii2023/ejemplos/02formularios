<?php

namespace app\models;

use yii\base\Model;

/**
 * Description of Formulario3
 */
class Formulario3 extends Model
{

    public ?string $nombre = null;
    public ?int $edad = null;
    public ?int $mes = null;
    public ?int $categoria = null;
    public ?string $dia = null;
    public array $aficiones = [];
    public array $opciones = [];
    public ?string $fechaAlta = null;


    public function attributeLabels(): array
    {
        return [
            "nombre" => "Nombre",
            "edad" => "Edad",
            "mes" => "Mes Uso",
            "categoria" => "Categoría Herramienta",
            "dia" => "Día Uso",
            "aficiones" => "Aficiones",
            "opciones" => "Opciones alquiler",
            "fechaAlta" => "Fecha de alta al sistema",
        ];
    }

    public function rules(): array
    {
        return [
            [["nombre", "edad",  "dia", "categoria", "mes", "aficiones", "opciones", "fechaAlta"], "safe"],
        ];
    }

    public function getMeses()
    {
        return [
            1 => 'Enero',
            2 => 'Febrero',
            3 => 'Marzo',
            4 => 'Abril',
            5 => 'Mayo',
            6 => 'Junio',
            7 => 'Julio',
            8 => 'Agosto',
            9 => 'Septiembre',
            10 => 'Octubre',
            11 => 'Noviembre',
            12 => 'Diciembre'
        ];
    }

    public function getDias()
    {
        return ["Domingo", "Lunes", "Miéroles", "Viernes"];
    }

    public function getCategorias()
    {
        return ["A", "B", "C"];
    }

    public function getMostraraficiones()
    {
        return [
            'lectura', 'deporte', 'tv', 'informatica',
        ];
    }

    public function getListaAficiones(): string
    {
        return join(",", $this->aficiones);
    }

    public function getMostraropciones()
    {
        return [
            'dia completo', 'con carga', 'limpieza incluída',
        ];
    }

    public function getListaOpciones(): string
    {
        return join(",", $this->opciones);
    }

    public function fechaEspana()
    {
        $originalDate = $this->fechaAlta;
        $newDate = date("d/m/Y", strtotime($originalDate));
        return $newDate;
    }
}
