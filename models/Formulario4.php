<?php

namespace app\models;

use yii\base\Model;

/**
 * Description of Formulario4
 */
class Formulario4 extends Model
{

    public ?string $nombre = null;
    public ?string $apellidos = null;
    public ?string $fechaNacimiento = null; // anterior a 31/12
    public ?string $correo = null;
    public ?string $poblacion = null;
    public array $mesesAcceso = [];



    public function attributeLabels(): array
    {
        return [
            "nombre" => "Nombre",
            "apellidos" => "apellidos",
            "mes" => "Fecha de nacimiento",
            "correo" => "Correo electrónico",
            "poblacion" => "Población",
            "mesesAcceso" => "Meses de acceso",

        ];
    }

    public function rules(): array
    {
        return [
            [["nombre", "apellidos",  "fechaNacimiento", "correo", "poblacion", "mesesAcceso"], "safe"],
            [['nombre', 'apellidos', 'poblacion'], 'string'],
            [['correo'], 'email'],
            [['fechaNacimiento'], 'date', 'format' => 'yyyy-MM-dd', 'max' => '2023-12-31'],
        ];
    }

    public function getPoblaciones(): array
    {
        return [
            "santander" => "SANTANDER",
            "torrelavega" => "TORRELAVEGA",
            "laredo" => "LAREDO",
            'astillero' => "ASTILLERO",
        ];
    }
    public function getMesesacceso(): array
    {
        return [
            "junio" => "Junio",
            "julio" => "Julio",
            "agosto" => "Agosto",
            'septiembre' => "Septiembre"
        ];
    }
    public function getListameses(): string
    {
        return join(",", $this->mesesAcceso);
    }
    public function fechaEspana()
    {
        $originalDate = $this->fechaNacimiento;
        $newDate = date("d/m/Y", strtotime($originalDate));
        return $newDate;
    }
}
