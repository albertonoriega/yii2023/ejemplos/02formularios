<?php

namespace app\models;

use DateTime;
use yii\base\Model;
use yii\db\ActiveRecord;

/**
 * Description of Formulario2
 */
class Formulario2 extends ActiveRecord
{

    public static function tableName()
    {
        return 'formulario2';
    }



    public function attributeLabels(): array
    {
        return [
            "dia" => "Día",
            "mes" => "Mes",
            "poblacion" => "Población",
        ];
    }

    public function rules(): array
    {
        return [
            [["dia", "mes", "poblacion"], "required", 'message' => 'El campo {attribute} es obligatorio'], // {attribute} te indica el nombre del campo
            [["dia"], 'integer'],
            [["mes", "poblacion"], 'string'],
            // [['dia'], 'number', 'min' => 1, 'max' => date('t')], // funcion fecha
            [['dia'], 'number', 'min' => 1, 'max' => $this->ultimoDia()],
        ];
    }

    public function ultimoDia()
    {
        $fecha = new \DateTime(); // clase de php de fechas
        $fecha->modify('last day of this month');
        $dia = $fecha->format('d');
        return $dia;
    }


    public function getMeses()
    {
        return [
            'Enero' => 'Enero',
            'Febrero' => 'Febrero',
            'Marzo' => 'Marzo',
            'Abril' => 'Abril',
            'Mayo' => 'Mayo',
            'Junio' => 'Junio',
            'Julio' => 'Julio',
            'Agosto' => 'Agosto',
            'Septiembre' => 'Septiembre',
            'Octubre' => 'Octubre',
            'Noviembre' => 'Noviembre',
            'Diciembre' => 'Diciembre'
        ];
    }

    public function getPoblaciones(): array
    {
        return [
            "santander" => "SANTANDER",
            "torrelavega" => "TORRELAVEGA",
            "laredo" => "LAREDO",
            'comillas' => "COMILLAS"
        ];
    }
}
