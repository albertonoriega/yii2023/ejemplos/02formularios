<?php

namespace app\models;

use yii\base\Model;

/**
 * Description of Formulario1
 */
class Formulario1 extends Model
{
    public ?int $numero1 = null;
    public ?int $numero2 = null;
    public ?string $descripcion = null;

    public function attributeLabels(): array
    {
        return [
            "numero1" => "Numero 1",
            "numero2" => "Numero 2",
            "descripcion" => "Descripción",
        ];
    }

    public function rules()
    {
        return [
            [["numero1", "numero2", "descripcion"], "required", 'message' => 'El campo {attribute} es obligatorio'], // {attribute} te indica el nombre del campo
            [["numero2", "numero1"], 'integer'],

            //[["numero1"], 'integer', 'max' => 99.99, 'min' => 0.1], // Mayor de 0 y menor de 100

            [["numero2"], 'compare', 'compareValue' => 0, 'operator' => '!=', 'type' => 'number'], // distinto de cero

            // Rules con error personalizado
            [['numero1'], function ($atributo) {
                if (!($this->$atributo > 0 && $this->$atributo < 100)) {
                    return $this->addError($atributo, 'El número debe estar entre 0 y 100');
                }
            }],
        ];
    }

    public function suma()
    {
        $operacion = $this->numero1 + $this->numero2;
        return $operacion;
    }
    public function resta()
    {
        $operacion = $this->numero1 - $this->numero2;
        return $operacion;
    }
    public function producto()
    {
        $operacion = $this->numero1 * $this->numero2;
        return $operacion;
    }
    public function division()
    {
        $operacion = $this->numero1 / $this->numero2;
        return $operacion;
    }

    public function operaciones($operacion)
    {
        switch ($operacion) {
            case '+':
            case 'suma':
                //$resultado = $this->numero1 + $this->numero2;
                $resultado = $this->suma();
                break;
            case '-':
            case 'resta':
                //$resultado = $this->numero1 - $this->numero2;
                $resultado = $this->resta();
                break;
            case '*':
            case 'producto':
            case 'multiplicacion':
                //$resultado = $this->numero1 * $this->numero2;
                $resultado = $this->producto();
                break;
            case '/':
            case 'division':
            case 'cociente':
                //$resultado = $this->numero1 / $this->numero2;
                $resultado = $this->division();
                break;

            default:
                $resultado = 'Operación inválida';
                break;
        }
        return $resultado;
    }

    /**
     * contarVocales
     *
     * @return int numero de vocales
     */
    public function contarVocales()
    {
        $vocales = ['a', 'e', 'i', 'o', 'u'];
        $contador = 0;
        $descripcionArray = str_split(strtolower($this->descripcion));

        for ($i = 0; $i < count($descripcionArray); $i++) {
            for ($j = 0; $j < count($vocales); $j++) {
                if ($descripcionArray[$i] == $vocales[$j]) {
                    $contador++;
                }
            }
        }

        return $contador;
    }

    public function contarVocales1()
    {
        $vector = str_split(strtolower($this->descripcion));
        $vocales = ['a', 'e', 'i', 'o', 'u'];
        $contador = 0;
        foreach ($vector as $caracter) {
            if (in_array($caracter, $vocales)) {
                $contador++;
            }
        }
        return $contador;
    }

    public function contarVocales2()
    {
        $vocales = substr_count(strtolower($this->descripcion), 'a') + substr_count(strtolower($this->descripcion), 'e') + substr_count(strtolower($this->descripcion), 'i') + substr_count(strtolower($this->descripcion), 'o') + substr_count(strtolower($this->descripcion), 'u');
        return $vocales;
    }
}
